---
downloads: ~/.ansible-downloads/

configure_dotfiles: true
configure_terminal: true
configure_osx: true

configure_zsh: true
# task: oh-my-zsh-setup
zsh_change_user_shell: yes

# Set to 'true' to configure the Dock via dockutil.
configure_dock: false
dockitems_remove:
  - Launchpad
  - TV
  - Podcasts
  - "App Store"
dockitems_persist:
  - name: "Visual Studio Code"
    path: "/Applications/Visual\ Studio\ Code.app/"
    pos: 5

configure_sudoers: false
sudoers_custom_config: ""
# Example:
# sudoers_custom_config: |
#   # Allow users in admin group to use sudo with no password.
#   %admin ALL=(ALL) NOPASSWD: ALL

# dotfiles_repo: https://github.com/geerlingguy/dotfiles.git
dotfiles_repo: https://gitlab.com/technologistpro/dotfiles.git
dotfiles_repo_accept_hostkey: true
dotfiles_repo_local_destination: ~/Documents/workspace/dotfiles
dotfiles_files:
  - .p10k.zsh
  - .zshrc
  # - .gitignore
  # - .inputrc
  # - .osx
  # - .vimrc

### Pyevn
# Update .bashrc and .zshrc shell scripts
pyenv_init_shell: false

# Detect existing install
pyenv_detect_existing_install: true

# Latest Python versions
# pyenv_python37_version: "3.7.10"
# pyenv_python38_version: "3.8.7"
pyenv_python39_version: "3.9.7"

# Python versions to install
# pyenv_python_versions:
#   - "{{ pyenv_python37_version }}"
#   - "{{ pyenv_python38_version }}"

pyenv_python_versions:
  - "{{ pyenv_python39_version }}"

pyenv_global: "{{ pyenv_python39_version }} system"

configure_powerline: true
# role: sicruse.powerline-fonts
powerline_fonts:
  - UbuntuMono
  # - Menlo
  # - Inconsolata

homebrew_taps:
  - homebrew/core
  - homebrew/cask
  - weaveworks/tap # Needed for eksctl
  - aws/tap # Needed for aws-sam-cli

# homebrew_cask_appdir: /Applications
homebrew_upgrade_all_packages: no

homebrew_installed_packages:
  # - ansible # Installed via Pip.
  - autoconf
  - aws-sam-cli
  - bash-completion
  - bash-git-prompt
  - bfg # Remove large files or passwords from Git history like git-filter-branch
  - coreutils
  - cowsay
  - docx2txt
  - doxygen
  - eksctl # Requires brew tap weaveworks/tap
  - exiftool #Perl lib for reading and writing EXIF metadata
  - ffmpeg # Play, record, convert, and stream audio and video
  - gawk # Needed by at least ansi2html.sh
  - gettext
  - gifsicle
  - git
  - gnu-sed # Needed by ansi2html.sh
  - go
  - gpg
  - handbrake
  - httpie
  - httping
  - hugo # Configurable static site generator
  - icdiff # Awesome diff -y
  - imagemagick
  # - imageoptim-cli # was not working with M1
  - ioping
  - iperf
  - jq # Lightweight and flexible command-line JSON processor
  - lastpass-cli
  - lftp
  - libdvdcss # Access DVDs as block devices without the decryption
  - libevent
  - mcrypt
  - nmap
  - node
  - nvm
  - openssl
  - pandoc
  - plantuml
  - pv
  - pyenv
  - readline
  - rename
  - sqlite
  - ssh-copy-id
  - tcping
  - tree
  - wget
  - youtube-dl
  - wrk
  # - zsh-history-substring-search
  # - zsh-syntax-highlighting

homebrew_cask_apps:
  - 1password
  - appcleaner
  - authy
  - balenaetcher
  - burn
  - caffeine
  - chromedriver
  - cyberduck
  - docker
  - flux
  - handbrake
  - imageoptim
  - iterm2
  - lastpass
  - licecap
  - macvim
  - meld
  - mysqlworkbench
  - obs
  - omnidisksweeper
  - postman
  - radio-silence
  - rar
  - sequel-ace
  - transmit
  - tunnelblick
  - visual-studio-code
  # - vlc
  # - firefox
  # - google-chrome

# See `geerlingguy.mas` role documentation for usage instructions.
mas_installed_apps: []
mas_email: ""
mas_password: ""

osx_script: "~/.osx --no-restart"

# Install packages from other package managers.
# Note: You are responsible for making sure the required package managers are
# installed, eg. through homebrew.
composer_packages:
  []
  # - name: drush
  #   state: present # present/absent, default: present
  #   version: "^8.1" # default: N/A
gem_packages:
  []
  # - name: bundler
  #   state: present # present/absent/latest, default: present
  #   version: "~> 1.15.1" # default: N/A
npm_packages:
  []
  # - name: webpack
  #   state: present # present/absent/latest, default: present
  #   version: "^2.6" # default: N/A
pip_packages:
  ["aws-mfa", "ipython"]
  # - name: mkdocs
  #   state: present # present/absent/latest, default: present
  #   version: "0.16.3" # default: N/A

# Glob pattern to ansible task files to run after all other tasks are finished.
post_provision_tasks: []
