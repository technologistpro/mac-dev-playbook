# Mac Development Ansible Playbook

This playbook installs and configures most of the software I use on my Mac

_Inspired by_:
[mac-dev-playbook](https://github.com/geerlingguy/mac-dev-playbook)

## Installation

Assuming you have an admin account, and a regular account (e.g. john)

Create a group ‘brew’ and add admin and regular accounts as members

### Grant sudo to regular account

Need sudo to install some packages (admin on mac is not needed)

### Install Brew as admin account and apply proper permissions

```console
$ su - macadmin
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
$ brew doctor
$ sudo chgrp -R brew $(brew --prefix)/*
$ sudo chmod -R g+w $(brew --prefix)/*
$ sudo chown john -R g+w $(brew --prefix)/*
```

## Install Ansible

1. Ensure Apple's command line tools are installed (`xcode-select --install` to launch the installer).

1. [Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/index.html):

   1. Run the following command to add Python 3 to your PATH: `export PATH="$HOME/Library/Python/3.9/bin:/opt/homebrew/bin:$PATH"`
   2. Upgrade Pip: `sudo pip3 install --upgrade pip`
   3. Install Ansible: `pip3 install ansible`

### This playbook (main.yml) does the following as configured in this repo:

1. Install applications via Homebrew (including Casks) specified in default_config.yml
2. Install dotfiles
3. Install powerline-fonts
4. Install pyenv
5. Install Mac Apple Store Apps specified in default_config.yml
6. Setup sudoers
7. Configure Terminal
8. Setup Oh My Zsh
9. Configure OSX settings
10. Install Extra Packages (e.g. pip modules, npm modules)

### Run this ansible playbook

1. Clone this repository to your local drive.
2. Run `$ ansible-galaxy install -r requirements.yml` inside this directory to install required Ansible roles.
3. Run `ansible-playbook main.yml -i inventory -K` inside this directory. Enter your account password when prompted.

> Note: If some Homebrew commands fail, you might need to agree to Xcode's license or fix some other Brew issue. Run `brew doctor` to see if this is the case.

### Use with a remote Mac

You can use this playbook to manage other Macs as well; the playbook doesn't even need to be run from a Mac at all! If you want to manage a remote Mac, either another Mac on your network, or a hosted Mac like the ones from [MacStadium](https://www.macstadium.com), you just need to make sure you can connect to it with SSH:

1. (On the Mac you want to connect to:) Go to System Preferences > Sharing.
2. Enable 'Remote Login'.

> You can also enable remote login on the command line:
>
>     sudo systemsetup -setremotelogin on

Then edit the `inventory` file in this repository and change the line that starts with `127.0.0.1` to:

```
[ip address or hostname of mac]  ansible_user=[mac ssh username]
```

If you need to supply an SSH password (if you don't use SSH keys), make sure to pass the `--ask-pass` parameter to the `ansible-playbook` command.

### Running a specific set of tagged tasks

You can filter which part of the provisioning process to run by specifying a set of tags using `ansible-playbook`'s `--tags` flag. The tags available are `dotfiles`, `homebrew`, `mas`, `extra-packages`, `ohmyzsh`, `powerline-fonts`, `pyenv` and `osx`.

    ansible-playbook main.yml -i inventory -K --tags "dotfiles,ohmyzsh"

## Overriding Defaults

Not everyone's development environment and preferred software configuration is the same.

You can override any of the defaults configured in `default.config.yml` by creating a `config.yml` file and setting the overrides in that file. For example, you can customize the installed packages and apps with something like:

    homebrew_installed_packages:
      - cowsay
      - git
      - go

    mas_installed_apps:
      - { id: 498486288, name: "Quick Resizer" }
      - { id: 557168941, name: "Tweetbot" }
      - { id: 497799835, name: "Xcode" }

    composer_packages:
      - name: hirak/prestissimo
      - name: drush/drush
        version: '^8.1'

    gem_packages:
      - name: bundler
        state: latest

    npm_packages:
      - name: webpack

    pip_packages:
      - name: mkdocs

Any variable can be overridden in `config.yml`; see the supporting roles' documentation for a complete list of available variables.

## Included Applications / Configuration (Default)

Applications (installed with Homebrew Cask)

Packages (installed with Homebrew)

dotfiles are installed into the current user's home directory. You can disable dotfiles management by setting `configure_dotfiles: no` in your configuration.

### Things that still need to be done manually

1. Set iTerm Preferences

Preferences - Profiles - Window - Tranparency

> **(Optional - not needed when using powerlevel10k)**

> - Preferences - Profiles - Colors - Color Presets : Tango Dark
> - Preferences - Profiles - Text - Font : Menlo

1. Install AWS CLI v2
   https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html

1. Configure [aws-mfa](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa.html)

1. Set JJG-Term as the default Terminal theme (it's installed, but not set as default automatically).

1. Install App Store apps

- iMovie
- Microsoft Remote Desktop
- Microsoft To Do
